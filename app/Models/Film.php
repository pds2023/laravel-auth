<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Film extends Model
{
    use HasFactory;

    protected $table = 'film';

    protected $fillable = ['judul', 'ringkasan', 'tahun','poster','genre_id'];

    public function genre(): HasMany
    {
        return $this->hasMany(Genre::class, 'genre_id');
    }

    public function cast()
    {
        return $this->belongsToMany(Cast::class, 'peran');
    }

    public function user()
    {
        return $this->belongsToMany(User::class, 'kritik');
    }

    
}
