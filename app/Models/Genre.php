<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Genre extends Model
{
    use HasFactory;

    protected $table = 'genre';

    protected $fillable = ['name'];

    public function film(): BelongsTo
    {
        return $this->belongsTo(Film::class, 'genre_id');
    }



}
